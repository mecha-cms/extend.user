User Extension for Mecha
========================

Release Notes
-------------

### 1.11.4

 - Added `target` attribute to the log-in form.

### 1.11.3

 - Fix broken user profile page that always redirect to the log-in page.

### 1.11.2

 - Default log-in and log-out system is now enabled by this extension.
 - Added log-in attempts counter.
 - Added routes to the user profile page.
 - Changed `$` property to `author` to store the human-friendly user name.
